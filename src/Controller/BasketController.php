<?php

namespace App\Controller;

use App\Entity\Basket;
use App\Repository\BasketRepository;
use App\Repository\ProductRepository;
use App\Service\Product;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @Route("/basket")
 */
class BasketController extends AbstractController
{
    private $entityManager;
    private $flashBag;
    private $router;
    private $basketRepository;
    private $productRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        ProductRepository $productRepository,
        FlashBagInterface $flashBag,
        RouterInterface $router,
        BasketRepository $basketRepository)
    {
        $this->entityManager = $entityManager;
        $this->flashBag = $flashBag;
        $this->router = $router;
        $this->basketRepository = $basketRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * @Route("/", name="basket_index")
     */
    public function index(TokenStorageInterface $tokenStorage, \App\Service\Basket $basketService)
    {
        $user = $tokenStorage->getToken()->getUser();
        $baskets = $this->basketRepository->findBy(['user' => $user, 'completed' => false], ['time' => 'DESC']);
        $total = $basketService->calculateTotal($baskets);

        return $this->render('basket.html.twig', [
            'baskets' => $baskets,
            'total' => $total
        ]);
    }

    /**
     * @Route("/add", name="basket_add")
     * @Security("is_granted('ROLE_USER')")
     */
    public function add(Request $request, TokenStorageInterface $tokenStorage, Product $productService, \App\Service\Basket $basketService)
    {
        $user = $tokenStorage->getToken()->getUser();
        $amount = $request->request->get("amount");
        $productId = $request->request->get("productId");

        if($amount == 0 || $amount < 0){
            $this->flashBag->add('warning', "You must add at least one product");
            return new RedirectResponse($this->router->generate('home'));
        }

        $product = $productService->getById($productId);

        $basket = $this->alreadyAdded($user, $product);

        if (count($basket) > 0) {
            $amount = $basket[0]->getAmount() + $amount;
            $basketService->updateAmount($basket[0], $amount);
            $this->flashBag->add('notice', $product->getName() . " was updated to basket");
        } else {
            $basket = new Basket();
            $basketService->add($basket, $user, $product, $amount);
            $this->flashBag->add('notice', $product->getName() . " was added to basket");
        }

        return new RedirectResponse($this->router->generate('home'));
    }

    /**
     * @Route("/remove/{id}", name="basket_remove")
     * @param $id
     */
    public function remove(Basket $basket)
    {
        $this->entityManager->remove($basket);
        $this->entityManager->flush();

        $this->flashBag->add('notice', $basket->getProduct()->getName()." was removed from basket.");

        return new RedirectResponse($this->router->generate('basket_index'));
    }

    private function alreadyAdded($user, $product)
    {
        return $this->basketRepository->findBy(['user' => $user, 'product' => $product, 'completed' => false]);
        //return $this->basketRepository->alreadyAdded($user, $product);
    }
}