<?php


namespace App\Controller;


use App\Repository\BasketRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class HomeController extends AbstractController
{
    private $productRepository;
    private $basketRepository;

    public function __construct(
        ProductRepository $productRepository,
        BasketRepository $basketRepository)
    {
        $this->productRepository = $productRepository;
        $this->basketRepository = $basketRepository;
    }
    /**
     * @Route("/", name="home")
     */
    public function index(TokenStorageInterface $tokenStorage)
    {
        return $this->render('home.html.twig', [
            'products' => $this->productRepository->findBy([], ['time'=>'DESC'])
        ]);
    }
}