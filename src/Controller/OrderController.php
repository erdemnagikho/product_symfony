<?php


namespace App\Controller;


use App\Entity\Basket;
use App\Entity\Order;
use App\Entity\OrderDetail;
use App\Entity\User;
use App\Event\OrderEvent;
use App\Event\UserRegisterEvent;
use App\Repository\BasketRepository;
use App\Repository\OrderRepository;
use App\Repository\StatusRepository;
use App\Service\Address;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @Route("/order")
 */
class OrderController extends AbstractController
{
    private $statusRepository;
    private $basketRepository;
    private $orderRepository;
    private $entityManager;
    private $flashBag;
    private $router;

    public function __construct(
        StatusRepository $statusRepository,
        BasketRepository $basketRepository,
        OrderRepository $orderRepository,
        FlashBagInterface $flashBag,
        EntityManagerInterface $entityManager,
        RouterInterface $router)
    {
        $this->statusRepository = $statusRepository;
        $this->basketRepository = $basketRepository;
        $this->orderRepository = $orderRepository;
        $this->flashBag = $flashBag;
        $this->router = $router;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="order_index")
     */
    public function index(TokenStorageInterface $tokenStorage)
    {
        $user = $tokenStorage->getToken()->getUser();

        if ($this->isGranted('ROLE_ADMIN')) {
            return $this->render('admin/order/index.html.twig', [
                'orders' => $this->orderRepository->findBy([], ['time' => 'DESC'])
            ]);
        } else {
            return $this->render('orders.html.twig', [
                'orders' => $this->orderRepository->findBy(['user' => $user], ['time' => 'DESC'])
            ]);
        }
    }

    /**
     * @Route("/complete", name="order_complete")
     * @Security("is_granted('ROLE_USER')")
     */
    public function complete(
        Request $request,
        TokenStorageInterface $tokenStorage,
        \App\Service\Address $addressService,
        \App\Service\Order $orderService,
        \App\Service\OrderDetail $orderDetailService,
        \App\Service\Basket $basketService,
        \App\Service\Product $productService,
        EventDispatcherInterface $eventDispatcher)
    {
        $user = $tokenStorage->getToken()->getUser();
        $city = $request->request->get("city");
        $county = $request->request->get("county");
        $fullAddress = $request->request->get("address");
        $total = $request->request->get("total");
        $status = $this->statusRepository->find(1);

        $address = new \App\Entity\Address();
        $address = $addressService->add($address, $user, $city, $county, $fullAddress);

        $order = new Order();
        $order = $orderService->add($order, $user, $address, $status, $total);

        $baskets = $this->basketRepository->findBy(['user' => $user, 'completed' => false], ['time' => 'DESC']);

        $orderDetailService->add($order, $baskets);

        $basketService->updateCompleted($baskets);

        $productService->updateStocks($baskets);

        $this->flashBag->add('notice', " Your order was completed");

        $orderEvent = new OrderEvent($user);
        $eventDispatcher->dispatch(OrderEvent::NAME,$orderEvent);

        return new RedirectResponse($this->router->generate('basket_index'));
    }

    /**
     * @Route("/cancel/{id}", name="order_cancel")
     * @Security("is_granted('cancel', order)", message="Access denied")
     * @param $id
     */
    public function cancel(Order $order)
    {
        if ($order->getStatus()->getName() == "Pending") {
            $status = $this->statusRepository->find(3);

            $order->setStatus($status);

            $this->entityManager->persist($order);
            $this->entityManager->flush();

            $this->flashBag->add('notice', "Order #" . $order->getId() . " was cancelled");
        } else {
            $this->flashBag->add('notice', "You can cancel order when it's status pending");
        }

        return new RedirectResponse($this->router->generate('order_index'));
    }

    /**
     * @Route("/update/{id}", name="order_update")
     * @Security("is_granted('ROLE_ADMIN')")
     * @param $id
     */
    public function update(Request $request, Order $order, EventDispatcherInterface $eventDispatcher)
    {
        $statusId = $request->request->get("statusId");

        $status = $this->statusRepository->find($statusId);

        $order->setStatus($status);

        $this->entityManager->persist($order);
        $this->entityManager->flush();

        $this->flashBag->add('notice', "Order #" . $order->getId() . " was ". $status->getName());

        $orderEvent = new OrderEvent($order->getUser());
        $eventDispatcher->dispatch(OrderEvent::ORDER_STATUS,$orderEvent);

        return new RedirectResponse($this->router->generate('order_index'));
    }
}