<?php


namespace App\Controller;


use App\Entity\Product;
use App\Form\PostType;
use App\Repository\ProductRepository;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * @Route("/product")
 */
class ProductController extends AbstractController
{
    private $productRepository;
    private $formFactory;
    private $entityManager;
    private $router;
    private $flashBag;
    private $authorizationChecker;

    public function __construct(
        ProductRepository $productRepository,
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        RouterInterface $router,
        FlashBagInterface $flashBag,
        AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->productRepository = $productRepository;
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->flashBag = $flashBag;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @Route("/", name="product_index")
     */
    public function index()
    {
        if($this->isGranted('ROLE_ADMIN')){
            return $this->render('admin/product/index.html.twig', [
                'products' => $this->productRepository->findBy([], ['time'=>'DESC'])
            ]);
        } else{
            return $this->render('home.html.twig', [
                'products' => $this->productRepository->findBy([], ['time'=>'DESC'])
            ]);
        }
    }

    /**
     * @Route("/add", name="product_add")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function add(Request $request, TokenStorageInterface $tokenStorage,FileUploader $fileUploader)
    {
        $user = $tokenStorage->getToken()
            ->getUser();

        $product = new Product();
        $product->setUser($user);
        $product->setTime(new \DateTime());

        $form = $this->formFactory->create(PostType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $product->getImage();
            $fileName = $fileUploader->upload($file);
            $product->setImage($fileName);

            $this->entityManager->persist($product);
            $this->entityManager->flush();

            return new RedirectResponse($this->router->generate('product_index'));
        }

        return $this->render('admin/product/add.html.twig', [
            'form' => $form->createView(),
            'image'=> ''
        ]);
    }

    /**
     * @Route("/edit/{id}", name="product_edit")
     * @Security("is_granted('ROLE_ADMIN')")
     * @param $id
     */
    public function edit(Product $product, Request $request, FileUploader $fileUploader)
    {
        /*
        if(!$this->authorizationChecker->isGranted('edit', $product)) {
            throw new UnauthorizedHttpException();
        }
        */

        $form = $this->formFactory->create(PostType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $product->getImage();
            $fileName = $fileUploader->upload($file);
            $product->setImage($fileName);

            $this->entityManager->flush();

            return new RedirectResponse($this->router->generate('product_index'));
        }

        return $this->render('admin/product/add.html.twig', [
            'form' => $form->createView(),
            'image'=> $product->getImage()
        ]);
    }

    /**
     * @Route("/delete/{id}", name="product_delete")
     * @Security("is_granted('delete', product)", message="Access denied")
     * @param $id
     */
    public function delete(Product $product)
    {
        /*
        if(!$this->authorizationChecker->isGranted('delete', $product)) {
            throw new UnauthorizedHttpException();
        }
        */

        $this->entityManager->remove($product);
        $this->entityManager->flush();

        $this->flashBag->add('notice', $product->getName()." was deleted");

        return new RedirectResponse($this->router->generate('product_index'));
    }

    /**
     * @Route("/{id}", name="product_single")
     * @param $id
     */
    public function product(Product $product)
    {
        return $this->render('product_single.html.twig', [
            'product' => $product
        ]);
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }
}