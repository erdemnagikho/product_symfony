<?php


namespace App\Form;


use App\Entity\Product;
use Doctrine\DBAL\Types\FloatType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,
                ['label' => false, 'attr' => ['placeholder' => 'Please enter product name']]
            )
            ->add('code', TextType::class,
                ['label' => false, 'attr' => ['placeholder' => 'Please enter product code']]
            )
            ->add('price', NumberType::class,
                ['label' => false, 'attr' => ['placeholder' => 'Please enter product price']]
            )
            ->add('stock', TextType::class,
                ['label' => false, 'attr' => ['placeholder' => 'Please enter product stock']]
            )
            ->add('image', FileType::class, array('data_class' => null))
            ->add('description', TextareaType::class,
                ['label' => false, 'attr' => ['placeholder' => 'Please enter product description']])
            ->add('save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class
        ]);
    }
}