<?php


namespace App\Mailer;


use App\Entity\User;
use Twig\Environment;

class Mailer
{
    private $mailer;
    private $environment;
    private $mailFrom;

    public function __construct(\Swift_Mailer $mailer, Environment $environment, string $mailFrom)
    {
        $this->mailer = $mailer;
        $this->environment = $environment;
        $this->mailFrom = $mailFrom;
    }

    public function sendConfirmationEmail(User $user)
    {
        $body = $this->environment->render('email/registration.html.twig', [
            'user' => $user
        ]);

        $message = (new \Swift_Message())
            ->setSubject("Welcome To Shopping")
            ->setFrom($this->mailFrom)
            ->setTo($user->getEmail())
            ->setBody($body, "text/html");

        $this->mailer->send($message);
    }

    public function sendOrderEmail($user)
    {
        $body = $this->environment->render('email/order.html.twig', [
            'user' => $user
        ]);

        $message = (new \Swift_Message())
            ->setSubject("We have taken your order")
            ->setFrom($this->mailFrom)
            ->setTo($user->getEmail())
            ->setBody($body, "text/html");

        $this->mailer->send($message);
    }

    public function sendOrderStatusChangedEmail($user)
    {
        $body = $this->environment->render('email/order_status_changed.html.twig', [
            'user' => $user
        ]);

        $message = (new \Swift_Message())
            ->setSubject("Your order status changed")
            ->setFrom($this->mailFrom)
            ->setTo($user->getEmail())
            ->setBody($body, "text/html");

        $this->mailer->send($message);
    }
}