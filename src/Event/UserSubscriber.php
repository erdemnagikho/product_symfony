<?php


namespace App\Event;


use App\Mailer\Mailer;
use Symfony\Bridge\Monolog\Handler\SwiftMailerHandler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Twig\Environment;

class UserSubscriber implements EventSubscriberInterface
{
    private $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public static function getSubscribedEvents()
    {
        return [
          UserRegisterEvent::NAME => 'onUserRegister',
            OrderEvent::NAME => 'onUserOrder',
            OrderEvent::ORDER_STATUS => 'onUserOrderStatusChange'
        ];
    }

    public function onUserRegister(UserRegisterEvent $event)
    {
        $this->mailer->sendConfirmationEmail($event->getRegisteredUser());
    }

    public function onUserOrder(OrderEvent $event)
    {
        $this->mailer->sendOrderEmail($event->getOrderedUser());
    }

    public function onUserOrderStatusChange(OrderEvent $event)
    {
        $this->mailer->sendOrderStatusChangedEmail($event->getOrderedUser());
    }
}