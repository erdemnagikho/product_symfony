<?php


namespace App\Event;


use App\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class OrderEvent extends Event
{
    const NAME = 'user.order';
    const ORDER_STATUS = 'user.order.status';

    private $orderedUser;

    public function __construct($orderedUser)
    {
        $this->orderedUser = $orderedUser;
    }

    public function getOrderedUser()
    {
        return $this->orderedUser;
    }
}