<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadUsers($manager);
        //$this->loadProducts($manager);
    }

    private function loadProducts(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $product = new Product();
            $product->setName('Product ' . rand(0, 100));
            $product->setTime(new \DateTime('2021-01-01'));
            $product->setUser($this->getReference('john_doe'));
            $manager->persist($product);
        }

        $manager->flush();
    }

    private function loadUsers(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername("admin_doe");
        $user->setFullName("Admin Doe");
        $user->setEmail("admindoe@yahoo.com");
        $user->setPhone("+905550008181");
        $user->setPassword($this->passwordEncoder->encodePassword($user, "admin"));
        $user->setRoles([User::ROLE_ADMIN]);
        $this->addReference('admin_doe', $user);

        $manager->persist($user);
        $manager->flush();
    }
}
