<?php


namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;

class Address
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function add(\App\Entity\Address $address, $user,$city, $county, $fullAddress)
    {
        $address->setUser($user);
        $address->setCity($city);
        $address->setCounty($county);
        $address->setAddress($fullAddress);

        $this->entityManager->persist($address);
        $this->entityManager->flush();

        return $address;
    }
}