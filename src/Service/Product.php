<?php


namespace App\Service;


use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;

class Product
{
    private $productRepository;
    private $entityManager;

    public function __construct(ProductRepository $productRepository, EntityManagerInterface $entityManager)
    {
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
    }

    public function getById($id)
    {
        return $this->productRepository->find($id);
    }

    public function updateStocks($baskets)
    {
        foreach ($baskets as $basket) {
            $product = $basket->getProduct();
            $stock = $product->getStock();
            $stock = $stock - $basket->getAmount();
            $product->setStock($stock);

            $this->entityManager->persist($product);
            $this->entityManager->flush();
        }
    }
}