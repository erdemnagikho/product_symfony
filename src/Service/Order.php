<?php


namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;

class Order
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function add(\App\Entity\Order $order, $user, $address, $status, $total)
    {
        $order->setUser($user);
        $order->setAddress($address);
        $order->setStatus($status);
        $order->setTime(new \DateTime());
        $order->setType("door");
        $order->setTotal($total);

        $this->entityManager->persist($order);
        $this->entityManager->flush();

        return $order;
    }
}