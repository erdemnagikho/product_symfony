<?php


namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;

class OrderDetail
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function add($order, $baskets)
    {
        foreach ($baskets as $basket) {
            $orderDetail = new \App\Entity\OrderDetail();
            $orderDetail->setOrder($order);
            $orderDetail->setProduct($basket->getProduct());
            $orderDetail->setAmount($basket->getAmount());
            $orderDetail->setPrice($basket->getProduct()->getPrice());
            $orderDetail->setTotal($basket->getAmount() * $basket->getProduct()->getPrice());

            $this->entityManager->persist($orderDetail);
            $this->entityManager->flush();
        }
    }
}