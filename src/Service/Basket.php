<?php

namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;

class Basket
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function add(\App\Entity\Basket $basket, $user, $product, $amount)
    {
        $basket->setUser($user);
        $basket->setProduct($product);
        $basket->setAmount($amount);
        $basket->setCompleted(false);
        $basket->setTime(new \DateTime());

        $this->entityManager->persist($basket);
        $this->entityManager->flush();
    }

    public function updateAmount(\App\Entity\Basket $basket, $amount)
    {
        $basket->setAmount($amount);
        $basket->setTime(new \DateTime());

        $this->entityManager->persist($basket);
        $this->entityManager->flush();
    }

    public function updateCompleted($baskets)
    {
        foreach ($baskets as $basket) {
            $basket->setCompleted(true);
            $this->entityManager->persist($basket);
            $this->entityManager->flush();
        }
    }

    public function calculateTotal($baskets)
    {
        $total = 0;
        foreach ($baskets as $basket) {
            $total += $basket->getAmount() * $basket->getProduct()->getPrice();
        }
        return $total;
    }
}